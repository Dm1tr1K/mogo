$(document).ready(function () {
    $('.WeDo__spoiler-header').click(function (event) {
        if ($('.WeDo__spoilers-container').hasClass('one')) {
            $('.WeDo__spoiler-header').not($(this)).removeClass('active');
            $('.WeDo__spoiler-text').not($(this).next()).slideUp(300);
        }
        $(this).toggleClass('active').next().slideToggle(300);
    });
});